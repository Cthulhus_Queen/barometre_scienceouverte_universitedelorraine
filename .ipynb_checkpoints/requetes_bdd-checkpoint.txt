Les requêtes suivantes ont été saisies dans les différentes bases de données pour obtenir les listes de publications par année.

Il est conseillé de refaire chaque année l'intégralité des exports par année, car les bases de données sont mises à jour régulièrement.

Web of Science

Il faut d'abord vous connecter au Web of Science et vous créer un compte gratuit (ou vous connecter si vous avez déjà un compte). Ainsi, votre nom apparaîtra en haut à droite de l'écran.

Il faut ensuite faire une recherche par "Affiliation" et chercher votre établissement dans l'index. Puis ajouter le critère de recherche "Year Published" et commencer par l'année 2016.

Il faut télécharger les années une par une.

Enfin, depuis la liste de résultat, faire l'export en cliquant sur "export" puis "fast 5k".

Il faut enregistrer le fichier dans le dossier data > raw > 2016 (si c'est l'année 2016) et le nommer ainsi : wos_nomdeletablissement_2016


PubMed

Utiliser la recherche avancée pour trouver les publications d'un établissement.

Détail de l'extraction pour l'Université de Lorraine :
(("universite de lorraine"[Affiliation] OR "university of lorraine"[Affiliation] 
OR "university of lorraine and"[Affiliation] OR "universite lorraine"[Affiliation] 
OR "university lorraine"[Affiliation]) AND "2016"[Date - Publication] : "2016"[Date - Publication])

Cliquer sur "save" puis "all results" et choisir le format CSV et cliquer sur "create file". Sauvegarder tous les résultats au format CSV année par année comme dans l'exemple lorrain.


HAL-UL

/!\ Cette étape n'est plus nécessaire puisque le ou les codes collection HAL peuvent être transmis directement au MESR dans le fichier à envoyer lors d'une demande de BSO local.

La méthodologie suivante était utilisée précédement pour extraire les publications de l'établissement via ExtrHAL (URL : https://halur1.univ-rennes1.fr/ExtrHAL.php) ou via l'API. Exemple de requête : https://api.archives-ouvertes.fr/search/UNIV-LORRAINE/?q=docType_s:%28ART%20OR%20COMM%20OR%20OUV%20OR%20COUV%29%20AND%20%28publicationDate_tdate:[2016-01-01T00:00:00Z%20TO%202016-12-31T00:00:00Z]%20OR%20producedDate_tdate:[2016-01-01T00:00:00Z%20TO%202016-12-31T00:00:00Z]%29&rows=10000&wt=csv&fq=-status_i=111&fq=doiId_s:[%22%22%20TO%20*]&fl=doiId_s

Via ExtrHAL : dans l'identifiant HAL de la structure, mettre le numéro de l'établissement (pour la Lorraine, c'est 413289). Voir dans AureHAL pour trouver son code :
https://aurehal.archives-ouvertes.fr/structure/

Dans "choix des listes et dates de publications", choisir la période année par année, prendre tous les articles de revue (sauf vulgarisation), toutes les communications (sauf grand public), ouvrages ou chapitres ou direction d'ouvrages et toutes les autres productions puis valider. Il y a un petit temps de chargement, attendre qu'il soit terminé.

En haut de la liste de résultats, cliquez sur le bouton VOSviewerDOI. La liste des DOI s'affiche, il ne vous reste plus qu'à copier cette liste dans un fichier csv avec "doi" comme nom de colonne (pour avoir un exemple, voir les fichiers de l'Université de Lorraine).


APC
L'Université de Lorraine tient en interne un suivi des dépenses d'APC, avec le DOI de chaque publication pour laquelle un APC a été payé. Une partie de ces dépenses est à retrouver ici : https://zenodo.org/record/3491721#.Xkqrf0pCdPY

Pour le Baromètre, des fichiers csv à la même forme que les fichiers HAL sont générés à partir de ce suivi.

Si votre établissement ne réalise pas ce type de suivi, vous n'avez pas besoin de créer ces fichiers.


Lens.org
[CETTE BASE N'EST PLUS UTILISEE, SUITE A DES MISES A JOUR BEAUCOUP DE DOUBLONS SONT APPARUS]

https://www.lens.org/ : se créer un compte et se connecter pour accéder à toutes les fonctionnalités

Dans l'onglet "scholarly works", faire une première recherche par année via "Year published". Puis à partir des résultats, dans les facettes, choisir le menu "institution" et chercher le nom de son établissement, le cocher et cliquer sur "refine".

Institution : University of Lorraine

Dans le menu "export", choisir toutes les publications (attention c'est 1000 par défaut), exporter le champ DOI uniquement, au format CSV. On reçoit un mail quand l'export est prêt. L'export contient deux informations : le Lens ID et le DOI. Il faut séparer les deux informations au sein de la colonne, en sélectionnant la colonne puis en cliquant sur "Données", "Convertir", "Délimité", "Virgule" et terminer. Puis supprimer la colonne "Lens ID" et renommer la colonne "DOI" en "doi".

Dans les fichiers .csv pour chaque année, sélectionner l'unique colonne "doi", cliquer sur "Rechercher et sélectionner", "Rechercher des cellules", puis cocher "Cellules vides" et OK. Ensuite, cliquer sur "Supprimer", puis "Supprimer des lignes dans la feuille"